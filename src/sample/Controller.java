package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


public class Controller {

    @FXML
    private TextField x;
    @FXML
    private TextField y;
    @FXML
    private Label result;


    public void count(ActionEvent event) {

        if (!isNumber(x.getText())) {
            System.out.println("First parameter is not number.");
            return;
        }

        if (!isNumber(y.getText())) {
            System.out.println("Second parameter is not number.");
            return;
        }

        final double doubleX = toDouble(x.getText());
        final double doubleY = toDouble(y.getText());

        Button btn = (Button) event.getSource();
        switch (btn.getText()) {
            case "+":
                System.out.println("Add");
                result.setText(String.valueOf(add(doubleX, doubleY)));
                break;
            case "-":
                System.out.println("Substract");
                result.setText(String.valueOf(sub(doubleX, doubleY)));
                break;
            case "*":
                System.out.println("Multiply");
                result.setText(String.valueOf(multiply(doubleX, doubleY)));
                break;
            case "/":
                System.out.println("Divide");
                if (doubleY == 0)
                    result.setText("Divide by 0 is not allowed.");
                else
                    result.setText(String.valueOf(divide(doubleX, doubleY)));
                break;
            default:
                throw new IllegalArgumentException("Unrecognized operation.");

        }


    }


    private double add(double a, double b) {
        return a + b;
    }

    private double sub(double a, double b) {
        return a - b;
    }

    private double multiply(double a, double b) {
        return a * b;
    }

    private double divide(double a, double b) {
        if (b == 0) return Double.NaN;

        return a / b;
    }

    private boolean isNumber(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private double toDouble(String s) {
        return Double.valueOf(s);
    }

}
